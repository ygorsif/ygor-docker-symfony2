#!/bin/bash
 chown -R www-data:www-data /var/www/

 cd /var/www
 composer install
 rm -rf app/cache/*
 php app/console cache:clear --env=prod
 chmod -R 777 app/cache
 chmod -R 777 app/logs
 chmod -R 777 app/config/parameters.yml


    sed -i "s/AllowOverride None/AllowOverride All/g" /etc/apache2/apache2.conf
    a2enmod rewrite


source /etc/apache2/envvars
tail -F /var/log/apache2/* &
exec apache2 -D FOREGROUND
